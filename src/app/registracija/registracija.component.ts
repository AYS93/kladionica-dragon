import { DOCUMENT } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Korisnik } from '../model/korisnik';
import { Tiket } from '../model/tiket';

@Component({
  selector: 'app-registracija',
  templateUrl: './registracija.component.html',
  styleUrls: ['./registracija.component.scss']
})
export class RegistracijaComponent implements OnInit {

  visibleRegistration: boolean = false;
  visibleTekme: boolean = true;
  visibleForum: boolean = false;
  visibleTiket: boolean = false;
  regSuccess: boolean = false;
  korisnik!: Korisnik;

  forumThread: boolean = false;

  myScriptElement: HTMLScriptElement;

  tiket2!: Tiket;

   constructor(){

      this.myScriptElement = document.createElement("script");
      this.myScriptElement.src = "../../assets/forum.js";
      document.body.appendChild(this.myScriptElement);

   }

  ngOnInit(): void {
  }

  registrationInput(regForm: any) {
    this.korisnik = regForm.value;
    this.korisnik.role = "korisnik";
    this.korisnik.ulogovan = true;
    this.korisnik.krediti = 0;
    console.log(this.korisnik);
    //this.visibleRegistration = false;
    this.regSuccess = true;
  }

  loginInput(logForm: any) {
    this.korisnik = logForm.value;
    this.korisnik.email = "PROVERI MEJL";
    this.korisnik.role = "korisnik";
    this.korisnik.ulogovan = true;
    this.korisnik.krediti = 0;
    console.log(this.korisnik);
    this.visibleRegistration = false;
    this.tekme();
  }

  tekme(): void {
    this.visibleRegistration = false;
    this.visibleTekme = true;
    this.visibleForum = false;
    this.visibleTiket = false;
  }

  forum(): void {
    this.visibleRegistration = false;
    this.visibleTekme = false;
    this.visibleForum = true;
    this.visibleTiket = false;
  }

  registracija(): void {
    this.visibleRegistration = true;
    this.visibleTekme = false;
    this.visibleForum = false;
    this.visibleTiket = false;
  }

  tiket(): void {
    this.visibleRegistration = false;
    this.visibleTekme = false;
    this.visibleForum = false;
    this.visibleTiket = true;
    //this.tiket2.mecevi = new Array<String>();
  }

  homepage1(): void {
    this.korisnik.ulogovan = false;
    this.regSuccess = false;
    this.tekme();
  }

  fudbalLige: boolean = true;
  kosarkaLige: boolean = false;
  hokejLige: boolean = false;
  odbojkaLige: boolean = false;
  pikadoLige: boolean = false;
  tenisLige: boolean = false;
  stonitenisLige: boolean = false;
  rukometLige: boolean = false;

  fudbal(): void {
    this.fudbalLige = true;
    this.kosarkaLige = false;
    this.hokejLige = false;
    this.odbojkaLige = false;
    this.pikadoLige = false;
    this.tenisLige = false;
    this.stonitenisLige = false;
    this.rukometLige = false;
  }

  kosarka(): void {
    this.fudbalLige = false;
    this.kosarkaLige = true;
    this.hokejLige = false;
    this.odbojkaLige = false;
    this.pikadoLige = false;
    this.tenisLige = false;
    this.stonitenisLige = false;
    this.rukometLige = false;
  }

  hokej(): void {
    this.fudbalLige = false;
    this.kosarkaLige = false;
    this.hokejLige = true;
    this.odbojkaLige = false;
    this.pikadoLige = false;
    this.tenisLige = false;
    this.stonitenisLige = false;
    this.rukometLige = false;
  }
  odbojka(): void {
    this.fudbalLige = false;
    this.kosarkaLige = false;
    this.hokejLige = false;
    this.odbojkaLige = true;
    this.pikadoLige = false;
    this.tenisLige = false;
    this.stonitenisLige = false;
    this.rukometLige = false;
  }
  pikado(): void {
    this.fudbalLige = false;
    this.kosarkaLige = false;
    this.hokejLige = false;
    this.odbojkaLige = false;
    this.pikadoLige = true;
    this.tenisLige = false;
    this.stonitenisLige = false;
    this.rukometLige = false;
  }
  tenis(): void {
    this.fudbalLige = false;
    this.kosarkaLige = false;
    this.hokejLige = false;
    this.odbojkaLige = false;
    this.pikadoLige = false;
    this.tenisLige = true;
    this.stonitenisLige = false;
    this.rukometLige = false;
  }
  stonitenis(): void {
    this.fudbalLige = false;
    this.kosarkaLige = false;
    this.hokejLige = false;
    this.odbojkaLige = false;
    this.pikadoLige = false;
    this.tenisLige = false;
    this.stonitenisLige = true;
    this.rukometLige = false;
  }
  rukomet(): void {
    this.fudbalLige = false;
    this.kosarkaLige = false;
    this.hokejLige = false;
    this.odbojkaLige = false;
    this.pikadoLige = false;
    this.tenisLige = false;
    this.stonitenisLige = false;
    this.rukometLige = true;
  }

  dodajUTiket(): void {
    console.log("dodajUTiket() se poziva");
    document.querySelector("aktuelni_tiket")?.insertAdjacentHTML('beforeend', "<p>N.Djokovic - R.Nadal (1) kv. 1.45</p>");
    console.log("dodajUTiket() nakon dodavanja");
  }
}
