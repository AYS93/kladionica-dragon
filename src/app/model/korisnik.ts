export interface Korisnik {
    ulogovan: boolean;
    username: string;
    pass: string;
    role: string;
    email: string;
    krediti: number;
}