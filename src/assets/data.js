var threads = [
    {
        id: 1,
        title: "Tema 1",
        author: "kockar69",
        date: Date.now(),
        content: "Thread content",
        comments: [
            {
                author: "Dragan",
                date: Date.now(),
                content: "Hej, radi li ovo?"
            },
            {
                author: "zmajevratnik",
                date: Date.now(),
                content: "Hej, hej, valjda radi :)"
            }
        ]
    },
    {
        id: 2,
        title: "Tema 2",
        author: "kockar69",
        date: Date.now(),
        content: "Thread content",
        comments: [
            {
                author: "Dragan",
                date: Date.now(),
                content: "Kako da odigram tiket?"
            },
            {
                author: "kockar69",
                date: Date.now(),
                content: "To ne radi, danas sam probao i ja :("
            },
            {
                author: "milorad",
                date: Date.now(),
                content: "isto i kod mene >:("
            }
        ]
    }
]

var threads;
if (localStorage && localStorage.getItem('threads')) {
    threads = JSON.parse(localStorage.getItem('threads'));
} else {
    //threads = defaultThreads; 
    localStorage.setItem('threads', JSON.stringify(threads));
}