console.log(threads);

var container = document.querySelector('ol.forum-ol');
for (let thread of threads) {
    var html = `
        <li class="forum-row">
            <a href="#?id=${thread.id}" onclick="forumThreadShow();" (click)="forumThread = true">
            <h4 class="forum-title">
                ${thread.title}
            </h4>
            <div class="forum-bottom">
                <p class="forum-timestamp">
                    ${new Date(thread.date).toLocaleString()}
                </p>
                <p class="forum-commentCount">
                    ${thread.comments.length} komentara
                </p>
            </div>
            </a>
        </li>`;
    container.insertAdjacentHTML('beforeend', html);
}

function forumTest() {
    var x = document.getElementById("forumTest");
    if (x.style.display !== "none") {
        x.style.display = "none";
    }
}

function forumTest2() {
    var x = document.getElementById("forumTest");
    if (x.style.display === "none") {
        x.style.display = "block";
    }
}

function forumThreadShow() {
    var x = document.getElementById("forumThread");
    if (x.style.display === "none") {
        x.style.display = "block";
    }
    var y = document.getElementById("f1");
    if (y.style.display !== "none") {
        y.style.display = "none";
    }
}

function forumThreadHide() {
    var x = document.getElementById("forumThread");
    if (x.style.display !== "none") {
        x.style.display = "none";
    }
    var y = document.getElementById("f1");
    if (y.style.display === "none") {
        y.style.display = "block";
    }
}

//var id = window.location.search.slice(1);
var id = window.location.href.slice(-1);
var thread = threads.find(t => t.id == id);
console.log(thread);

var container = document.querySelector(".thread-header");
var html = `
    <h4 class="forum-title">
        ${thread.title}
    </h4>
    <div class="forum-bottom">
        <p class="forum-timestamp">
            ${new Date(thread.date).toLocaleString()}
        </p>
        <p class="forum-commentCount">
            ${thread.comments.length} komentara
        </p>
    </div>`;
 container.insertAdjacentHTML('beforeend', html);

var comments = document.querySelector(".thread-comments");
function addComment(comment) {
    var commentHtml = `
            <div class="thread-comment">
                <div class="thread-topComment">
                <p class="thread-user">
                    ${comment.author}
                </p>
                <p class="thread-commentTimeStamp">
                    ${new Date(thread.date).toLocaleString()}
                </p>
                </div>
                <div class="thread-commentContent">
                    ${comment.content}
                </div>
            </div>`;
    comments.insertAdjacentHTML('beforeend', commentHtml);
}

for (let comment of thread.comments) {
    addComment(comment);    
}

let btn = document.querySelector(".forumDugme");
btn.addEventListener('click', function() {
    let txt = document.querySelector(".forumKomentar");
    let comment = {
        author: 'korisnik123',
        date: Date.now(),
        content: txt.value
    }
    addComment(comment);
    txt.value = '';
    thread.comments.push(comment);
    localStorage.setItem('threads', JSON.stringify(threads));
})

